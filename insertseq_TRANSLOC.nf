#!/usr/bin/env nextflow

outDir = params.outDir

process adapter_transformation {
	input:
	set sampleID, val(ontarget), val(adapter_1F) from Channel.from( params.ontarget ).join( Channel.from( params.adapter_1F ) )
	val adapter_2 from params.adapter_2

	output:
	tuple sampleID, env(adapter_1m) into adapter1
	tuple sampleID, env(adapter_2m), env(ontarget_rc) into adapter2

	script:
	"""
	ontarget_rc=`echo $ontarget | tr ACGTacgt TGCATGCA | rev`
	adapter_1m=`echo $adapter_1F | tr acgt ACGT`
	adapter_2m=`echo $adapter_2 | tr acgt ACGT`
	"""
}

process stats {
	publishDir "$outDir/STATS", mode: 'copy'
	publishDir "REPORT", mode: 'copy', pattern: '*Dynamic_Histogram_Read_length.html'
	//publishDir "REPORT", mode: 'copy', pattern: '*LengthvsQualityScatterPlot_dot.png'
	publishDir "REPORT", mode: 'copy', pattern: '*_stats.csv'
	container 'juliamp/nanopack'

	input:
	set sampleID, file(reads) from Channel.fromPath( params.input ).map{ file -> tuple(file.baseName, file) }

	output:
	file("$sampleID/*.html") into html
	file("$sampleID/*.png") into png
	file("$sampleID/*.txt") into txt
	file("$sampleID/*.log") into log
	file("${sampleID}_stats.csv") into stats
	file("${sampleID}_*") into stats_plots
	tuple sampleID, env(nreads) into readNumber

	script:
	"""
    NanoStat --fastq $reads --outdir $sampleID --name $sampleID
    nanoQC -o $sampleID $reads
    NanoPlot --fastq $reads -o $sampleID

    cp $sampleID/Dynamic_Histogram_Read_length.html ${sampleID}_Dynamic_Histogram_Read_length.html
    #cp $sampleID/LengthvsQualityScatterPlot_dot.png ${sampleID}_LengthvsQualityScatterPlot_dot.png

    # Write stats.csv for report

    echo "Sample;${sampleID}" >> ${sampleID}_stats.csv
    echo "Mean read length;" | tr '\\n' '\\0' >> ${sampleID}_stats.csv
    grep "Mean read length:" $sampleID/NanoStats.txt | grep -o '[0-9]*,*[0-9]*\\.[0-9]*' >> ${sampleID}_stats.csv
    echo "Read length STDEV;" | tr '\\n' '\\0' >> ${sampleID}_stats.csv
    grep "STDEV read length:" $sampleID/NanoStats.txt | grep -o '[0-9]*,*[0-9]*\\.[0-9]*' >> ${sampleID}_stats.csv
    echo "Mean read quality;" | tr '\\n' '\\0' >> ${sampleID}_stats.csv
    grep "Mean read quality:" $sampleID/NanoStats.txt | grep -o '[0-9]*,*[0-9]*\\.[0-9]*' >> ${sampleID}_stats.csv
    echo "Number of initial reads;" | tr '\\n' '\\0' >> ${sampleID}_stats.csv
    grep "Number of reads:" $sampleID/NanoStats.txt | grep -o '[0-9]*,*[0-9]*\\.[0-9]*' >> ${sampleID}_stats.csv
    echo "Total sequenced bases;" | tr '\\n' '\\0' >> ${sampleID}_stats.csv
    grep "Total bases:" $sampleID/NanoStats.txt | grep -o '[0-9]*,*[0-9]*\\.[0-9]*' >> ${sampleID}_stats.csv

    nreads=`grep "Number of reads:" $sampleID/NanoStats.txt | grep -o '[0-9]*,*[0-9]*\\.[0-9]*' | tr '\\n' '\\0'`
    """
}

process paramsReport {
	input:
	set sampleID, val(ontarget), val(adapter_1F) from Channel.from( params.ontarget ).join( Channel.from( params.adapter_1F ) )
	val adapter_2 from params.adapter_2

	output:
	file("*_params.csv") into paramsreport

	script:
	"""
	echo "${sampleID};${adapter_1F};${adapter_2};${ontarget}" >> ${sampleID}_params.csv
	"""
}

process write_params {
	publishDir "REPORT", mode: 'copy', pattern: '*.csv'

	input:
	file(params) from paramsreport.collect()

	output:
	file("params.csv") into finalParams

	script:
	"""
	for f in $params; do
		cat \$f >> params.csv
	done
	"""
}

process paramsGeneralReport {
	publishDir "REPORT", mode: 'copy', pattern: 'params_general.csv'

	input:
	val outDir from params.outDir
	val umiType from params.umiType
	val umiClustering from params.umiClustering
	val minlen from params.minlen
	val maxlen from params.maxlen
	val id from params.id
	val ubs from params.ubs
	val error from params.error
	val genome from params.genome

	output:
	file("params_general.csv") into paramsgeneral

	script:
	"""
	g=`echo $genome | rev | cut -d'/' -f1 | rev`
	printf "UMI clustering;${umiClustering}\\nUMI type;${umiType}\\nminimum UMI length;${minlen}\\n\
	maximum UMI length;${maxlen}\\n	UMI clustering identity;${id}\\nUBS threshold;${ubs}\\n\
	Adapter trimming error;${error}\\nReference genome;\${g}\\nOutput directory;${outDir}\\n" >> params_general.csv
	"""
}

process quality_filtering {
	publishDir "$outDir/FILTERED", mode: 'copy'
	container 'juliamp/nanopack'

	input:
	set sampleID, file(reads) from Channel.fromPath( params.input ).map{ file -> tuple(file.baseName, file) }

	output:
	tuple sampleID, file("*_qualityFilt.fastq") into filtered
	tuple sampleID, file("*_qualityFilt.fastq") into filteredNOclustered
	tuple sampleID, env(filtreads) into filtNumber

	script:
	"""
	cat $reads | NanoFilt -l 200 -q 10 > ${sampleID}_qualityFilt.fastq
	filtreads=`grep -c "^+\$" ${sampleID}_qualityFilt.fastq`
	"""
}

process umi_extraction {
	publishDir "$outDir/UMI", mode: 'copy'
	container 'juliamp/pylibs'

	input:
	set sampleID, file(reads), val(adapter_1F) from filtered.join( adapter1 )
	val umiType from params.umiType

	output:
	tuple sampleID, file("*_extractedUMI.fasta") into extractedUmis
	tuple sampleID, env(extracted) into extractNumber

	script:
	if ( params.umiClustering )
	"""
	ONT_${umiType}_extract_umis.py --max-error 3 --adapter-length 250 \
		--fwd-context $adapter_1F \
		-o ${sampleID}_extractedUMI.fasta --tsv ${sampleID}_extractedUMI.tsv \
		$reads
	extracted=`grep -c "^>" ${sampleID}_extractedUMI.fasta`
	"""
	else
	"""
	touch dummy_extractedUMI.fasta
	extracted=0
	"""
}

process umi_clustering {
	//publishDir "$outDir/UMI", mode: 'copy', pattern: '*_cluster_consensus.fasta'
	container 'biomehub/vsearch'

	input:
	set sampleID, file(umis) from extractedUmis
	val minlen from params.minlen
	val maxlen from params.maxlen
	val id from params.id

	output:
	tuple sampleID, file("${sampleID}_vsearch_clusters*") into clusters
	file("*_clusters_consensus.fasta") into consensus

	script:
	if ( params.umiClustering )
	"""
	vsearch --clusterout_id --clusters ${sampleID}_vsearch_clusters \
		--centroids ${sampleID}_clusters_centroid.fasta \
		--consout ${sampleID}_clusters_consensus.fasta \
		--minseqlength $minlen --maxseqlength $maxlen --qmask none --threads 4 \
		--cluster_fast $umis --clusterout_sort --gapopen 0E/5I --gapext 0E/2I \
		--mismatch -8 --match 6 --iddef 0 --minwordmatches 0 --qmask none -id $id
	"""
	else
	"""
	touch ${sampleID}_vsearch_clusters_dummy
	touch dummy_clusters_consensus.fasta
	"""
}

process umi_get_ubs {
	input:
	set sampleID, file(cluster) from clusters.transpose()

	output:
	tuple sampleID, env(size), file(cluster) into clusterTuples
	tuple sampleID, env(size), val(cluster.baseName) into getUBS

	script:
	if ( params.umiClustering )
	"""
	size=`grep -c "^>" $cluster`
	"""
	else
	"""
	size=1000000
	"""
}

process umi_top_read {
	//publishDir "$outdir/UMI", mode: 'copy', pattern: '*'
	container 'juliamp/vsearchbiopy'

	input:
	set sampleID, val(size), file(cluster) from clusterTuples

	output:
	tuple sampleID, file("*consensus.fasta") optional true into consensusUmis
	tuple sampleID, file("top.fasta"), file("*seqs.fasta") optional true into topUmis

	when:
	size.toInteger() >= params.ubs

	script:
	if ( size.toInteger() == 1 )
	"""
	umi_to_seq.py -i $cluster -o ${cluster.baseName}_consensus.fasta
	"""
	else if ( params.umiClustering )
	"""
	umi_to_seq.py -i $cluster -o ${cluster.baseName}_seqs.fasta
	vsearch --sortbylength ${cluster.baseName}_seqs.fasta --topn 1 --output top.fasta
	sed -i 's/>/>centroid_/' top.fasta
	"""
	else
	"""
	touch dummy_consensus.fasta
	"""
}

process umi_polishing {
	//publishDir "$outdir/UMI", mode: 'copy', pattern: '*'
	container 'juliamp/racon-minimap'
	errorStrategy 'ignore'

	input:
	set sampleID, file(top), file(seqs) from topUmis

	output:
	tuple sampleID, file(seqs), file("racon_2.fa") into raconPolished

	script:
	"""
	minimap2 -x map-ont $top $seqs > ovlp_1.paf
	racon -t 4 -m 8 -x -6 -g -8 -w 500 --no-trimming $seqs ovlp_1.paf $top > racon_1.fa
	minimap2 -x map-ont racon_1.fa $seqs > ovlp_2.paf
	racon -t 4 -m 8 -x -6 -g -8 -w 500 --no-trimming $seqs ovlp_2.paf racon_1.fa > racon_2.fa
	"""
}

process umi_consensus {
	//publishDir "$outDir/UMI", mode: 'copy', pattern: '*'
	container 'juliamp/medaka'
	errorStrategy 'ignore'

	input:
	set sampleID, file(seqs), file(racon) from raconPolished

	output:
	tuple sampleID, file("*consensus.fasta") into medakaConsensus

	script:
	"""
	mini_align -i $seqs -r $racon -m -p map_1 -t 1
	medaka consensus map_1.bam consensus_1.hdf --threads 1 --model r941_min_high_g360 --chunk_len 6000
	mini_align -i $seqs -r $racon -m -p map_2 -t 1
	medaka consensus map_2.bam consensus_2.hdf --threads 1 --model r941_min_high_g360 --chunk_len 6000
	medaka stitch consensus_*.hdf ${seqs.baseName}_consensus.fasta
	"""
}

process umi_final {
	publishDir "$outDir/UMI", mode: 'copy', pattern: '*'
	beforeScript 'ulimit -Ss unlimited'

	input:
	set sampleID, file("*") from medakaConsensus.concat( consensusUmis ).groupTuple( remainder:true )

	output:
	tuple sampleID, file("${sampleID}_consensus.fasta") into finalConsensus

	script:
	"""
	find . -type l -name '*consensus.fasta' -exec cat {} + > ${sampleID}_consensus.fasta
	"""
}

process filtering_and_trimming {
	publishDir "$outDir/FILTERED", mode: 'copy'
	container 'genomicpariscentre/cutadapt:2.10'

	input:
	set sampleID, file(unclustered), file(clustered), val(adapter_2), val(ontarget) from filteredNOclustered.join( finalConsensus, remainder:true ).join( adapter2 )
	val error from params.error

	output:
	tuple sampleID, file("*_trimmed.fasta") into trimmed
	tuple sampleID, file("*_trimmed.fasta") into umiTrimmed
	//tuple sampleID, env(umis), env(trimmed) into umiNumber
	tuple sampleID, file(unclustered), file(clustered), file("*_trimmed.fasta") into distribution

	script:
	if ( params.umiClustering )
	"""
	ad=$adapter_2
	ot=$ontarget
	len=`expr \${#ad} + \${#ot}`
	cutadapt -e $error -O \$len --match-read-wildcards \
		--discard-untrimmed \
		-g $adapter_2...$ontarget --rc \
		-o ${sampleID}_trimmed.fasta \
		$clustered
	#umis=`grep -c "^>" $clustered`
	#trimmed=`grep -c "^>" ${sampleID}_trimmed.fasta`
	"""
	else
	"""
	ad=$adapter_2
	ot=$ontarget
	len=`expr \${#ad} + \${#ot}`
	cutadapt -e $error -O \$len --match-read-wildcards \
		--discard-untrimmed \
		-g $adapter_2...$ontarget --rc \
		-o ${sampleID}_trimmed.fasta \
		$unclustered
	#umis=`grep -c "^>" $unclustered`
	#trimmed=`grep -c "^>" ${sampleID}_trimmed.fasta`
	"""
}

/*
process plot_ubs {
	publishDir "REPORT", mode: 'copy', pattern: '*.html'
	container 'mchether/py3-bio:v4'
	beforeScript 'ulimit -Ss unlimited'

	input:
	set sampleID, val(sizes), val(names), file(trimmed) from getUBS.groupTuple( remainder:true ).join( umiTrimmed )

	output:
	tuple sampleID, file("${sampleID}_ubs.html") into ubsFile

	script:
	"""
	s=`echo $sizes | sed 's/,/ /g'| sed 's/\\[//g' | sed 's/\\]//g'`
	n=`echo $names | sed 's/,/ /g'| sed 's/\\[//g' | sed 's/\\]//g'`
	plot_UBS.py -s $sampleID -t $trimmed --sizes \$s --names \$n
	"""
}
*/

process distribution {
	publishDir "$outDir/FILTERED", mode: 'copy'
	publishDir "REPORT", mode: 'copy', pattern: '*.png'
	container 'mchether/py3-bio:v4'

	input:
	set sampleID, file(unclustered), file(clustered), file(trimmed) from distribution

	output:
	tuple sampleID, file("*_trimmingreport.png") into distributionOutput

	script:
	if ( params.umiClustering )
	"""
	trimming_report.py --pre $clustered --post $trimmed -o ${sampleID}_trimmingreport.png
	"""
	else
	"""
	paste - - - - < $unclustered | cut -f 1,2 | sed 's/^@/>/' | tr "\t" "\n" > ${sampleID}_unclust.fasta
	trimming_report.py --pre ${sampleID}_unclust.fasta --post $trimmed -o ${sampleID}_trimmingreport.png
	"""
}

/*
process filt_trim_report {
	publishDir "REPORT", mode: 'copy', pattern: '*.csv'

	input:
	set sampleID, val(nreads), val(filtreads), val(extracted), val(umis), val(trimmed), file(ubs), file(trim) from readNumber.join( filtNumber ).join( extractNumber ).join( umiNumber ).join( ubsFile ).join( distributionOutput )

	output:
	file("*_filttrim.csv") into filttrimreport

	script:
	"""
	printf "Sample;${sampleID}\n\
	Initial number of reads;${nreads}\n\
	Quality filtered reads;${filtreads}\n\
	Number of extracted UMIs;${extracted}\n\
	Number of consensus UMI sequences;${umis}\n\
	Number of filtered and trimmed reads;${trimmed}" >> ${sampleID}_filttrim.csv
	"""
}
*/

process mapping {
	publishDir "$outDir/MAPPED", mode: 'copy'
	container 'juliamp/minimap-samtools'

	input:
	set sampleID, file(reads) , file(genome) from trimmed.combine( Channel.fromPath( params.genome ) )

	output:
	tuple sampleID, file("*.sam") into mapped

	script:
	"""
	minimap2 -t 4 -ax map-ont $genome $reads -o ${sampleID}.sam
	"""
}

process map_filter {
	container 'juliamp/shapeilter'

	input:
	set sampleID, file(sam) from mapped

	output:
	tuple sampleID, file("*_filtered.sam") into filteredSam
	tuple sampleID, file("*_unselected.fasta") into bamUnselected

	script:
	"""
	filter_mapping.py -i $sam -p ${sampleID}_filtered.sam -s ${sampleID}_unselected.fasta
	"""
}

process create_bams {
	publishDir "$outDir/MAPPED", mode: 'copy', pattern: '*_filtered.sorted*'
	container 'juliamp/minimap-samtools'

	input:
	set sampleID, file(sam) from filteredSam

	output:
	tuple sampleID, file("*_filtered.sorted.bam"), file("*_filtered.sorted.bam.bai") into bamFiles
	tuple sampleID, env(mapped) into mappedCount

	script:
	"""
	samtools sort -@ 4 -O BAM -T tmp -o ${sampleID}_filtered.sorted.bam $sam
	samtools index -@ 4 ${sampleID}_filtered.sorted.bam ${sampleID}_filtered.sorted.bam.bai
	mapped=`samtools view -c ${sampleID}_filtered.sorted.bam`
	"""
}

process pre_peak_calling {
	publishDir "$outDir/PEAKS", mode: 'copy', pattern: '*_prePeakCalling_sorted_insertions.bed'
	publishDir "$outDir/PEAKS", mode: 'copy', pattern: '*_prePeakCalling.bed'
	container 'biocontainers/bedtools:v2.27.1dfsg-4-deb_cv1'

	input:
	set sampleID, file(bam), file(bai) from bamFiles

	output:
	tuple sampleID, file("*_prePeakCalling_sorted_insertions.bed") into prePeakCallingInsGenes
	tuple sampleID, file("*_prePeakCalling_sorted_insertions.bed") into prePeakCallingInsRepeats
	tuple sampleID, file(bam), file(bai), file("*_prePeakCalling_sorted_insertions.bed") into peakCalling
	tuple sampleID, env(prepeaks) into prepeaksCount

	script:
	"""
	bedtools bamtobed -i $bam > ${sampleID}_prePeakCalling.bed
	bedtools merge -c 4 -o count -i ${sampleID}_prePeakCalling.bed > ${sampleID}_prePeakCalling_insertions.bed
	sort -r -nk4 ${sampleID}_prePeakCalling_insertions.bed > ${sampleID}_prePeakCalling_sorted_insertions.bed
	prepeaks=`wc -l ${sampleID}_prePeakCalling_sorted_insertions.bed | cut -d" " -f1`
	"""
}

params.tdif = null
params.tgen = null

process peak_calling {
	publishDir "$outDir/PEAKS", mode: 'copy', pattern: "*"
	container 'juliamp/shapeilter'

	input:
	set sampleID, file(bam), file(bai), file(bed), file(ref_vector) from peakCalling.combine( Channel.fromPath( params.refVector ) )

	output:
	tuple sampleID, file("${sampleID}_filtered_peaksSite.bed") into filteredPeaksRepeats
	tuple sampleID, file("${sampleID}_filtered_peaksSite.bed") into filteredPeaksGenes
	tuple sampleID, file("${sampleID}_filtered_peaksSite.bed") into filteredPeaksTranslocations
	tuple sampleID, file("${sampleID}_filtered_peaks.bed") into filteredPeaksOut
	tuple sampleID, file("${sampleID}_filtered_peaks.bed") into peaksPlot
	tuple sampleID, file("${sampleID}_filtered_peaks_allInsertions.bed") into allRSSpeaks
	tuple sampleID, env(peaks) into peaksCount

	script:
	if( !params.tdif & !params.tgen )
	"""
	peak_calling.py --bam $bam --bed $bed \
		--oSite ${sampleID}_filtered_peaksSite.bed \
		--oFilt ${sampleID}_filtered_peaks.bed
	peaks=`wc -l ${sampleID}_filtered_peaks.bed | cut -d" " -f1`
	"""	
	else if ( !params.tdif )
	"""
	peak_calling.py --bam $bam --bed $bed \
		--oSite ${sampleID}_filtered_peaksSite.bed \
		--oFilt ${sampleID}_filtered_peaks.bed \
		--tgen $params.tgen
	peaks=`wc -l ${sampleID}_filtered_peaks.bed | cut -d" " -f1`
	"""
	else if ( !params.tgen )
   	"""
	peak_calling.py --bam $bam --bed $bed \
		--oSite ${sampleID}_filtered_peaksSite.bed \
		--oFilt ${sampleID}_filtered_peaks.bed \
		--tdif $params.tdif
	peaks=`wc -l ${sampleID}_filtered_peaks.bed | cut -d" " -f1`
	"""
	else
	"""
	peak_calling.py --bam $bam --bed $bed \
		--oSite ${sampleID}_filtered_peaksSite.bed \
		--oFilt ${sampleID}_filtered_peaks.bed \
		--tdif $params.tdif --tgen $params.tgen
	peaks=`wc -l ${sampleID}_filtered_peaks.bed | cut -d" " -f1`
	"""
}

process getOntarget {
	publishDir "$outDir/PEAKS", mode: 'copy', pattern: "*"

	input:
	val genome from params.genome
	val ontarget from params.ontarget
	set sampleID, val(ontarget) from Channel.from( params.ontarget )

	output:
	tuple sampleID, file("*_ontarget.sam") into ontargetSite

	script:
	"""
	echo ">onTarget" > ${sampleID}_on-target.fa
	echo $ontarget >> ${sampleID}_on-target.fa
        bbmap.sh in=${sampleID}_on-target.fa ref=$genome out=${sampleID}_ontarget.sam
	"""
}

process createOntarget_mab {
	publishDir "$outDir/PEAKS", mode: 'copy', pattern: "*"
	container 'juliamp/minimap-samtools'

	input: 
	set sampleID, file(sam) from ontargetSite

	output:
	tuple sampleID, file("*_on-target.sorted.bam") into onTargetBamFiles

	script:
	"""
	samtools sort -@ 4 -O BAM -T tmp -o ${sampleID}_on-target.sorted.bam $sam
	"""
}

process getTranslocations {
        publishDir "$outDir/PEAKS", mode: 'copy', pattern: "*"
	container 'biocontainers/bedtools:v2.27.1dfsg-4-deb_cv1'

	input: 
	set sampleID, file(bamFile) from onTargetBamFiles	
        set sampleID, file(filteredPeakBed) from filteredPeaksTranslocations

	output:
	set sampleID, file("*_on-target.bed"), file("*_filtered_peaks_TRANSLOCATIONS.bed") into translocation
	
	script:
	"""
	bedtools bamtobed -i $bamFile > ${sampleID}_on-target.bed
	chr=`awk -F " " '{ print \$1 }' ${sampleID}_on-target.bed`
	awk -v a="\$chr" '{ if (\$1 != a) { print } }' ${filteredPeakBed} > ${sampleID}_filtered_peaks_TRANSLOCATIONS.bed	
	"""
}

process TranslocationCircosPlot {
	publishDir "$outDir/PEAKS", mode: 'copy', pattern: "*"
	container 'msanvicente/biocircos'

	input:
	set sampleID, file(ontargetBed), file(inBed) from translocation
	val genome from params.genome

	output:
	file("*_circosPlot.html") into circosPlot

	script:
	"""
	genomename=`echo $genome | rev | cut -d '/' -f 1 | rev | cut -d '.' -f 1 | cut -d '_' -f 1`
        circosPlot.R $inBed $ontargetBed ${sampleID}_circosPlot.html \$genomename 
        """
}


/*
process karyoplot {
	publishDir "$outDir/OUTPUT", mode: 'copy', pattern: '*'
	publishDir "REPORT", mode: 'copy', pattern: '*'
	container 'juliamp/karyoplote-r'

	input:
	set sampleID, file(peaks) from peaksPlot
	val genome from params.genome

	output:
	file('*_chromplot.jpg') into karyoplot

	script:
	"""
	genomename=`echo $genome | rev | cut -d '/' -f 1 | rev | cut -d '.' -f 1 | cut -d '_' -f 1`
	karyoplot.R $peaks ${sampleID}_chromplot.jpg $sampleID \$genomename #ot st end
	"""
}

process annotate_repeats_Post {
	publishDir "$outDir/OUTPUT", mode: 'copy', pattern: '*'
	container 'juliamp/annotation'

	input:
	set sampleID, file(bed) from filteredPeaksRepeats

	output:
	file("*.html") optional true into postRepeatAnnot

	script:
	"""
	annotate.py -i $bed -o ${sampleID}_filteredPeaks_repeatsAnnotations.html --post
	"""
}

process annotate_genes_Pre {
	publishDir "$outDir/OUTPUT", mode: 'copy', pattern: '*'
	container 'rcavalcante/bioconductor_docker:RELEASE_3_11'

	input:
	set sampleID, file(bed) from prePeakCallingInsGenes

	output:
	file("*.tsv") into preGeneAnnot
	file("*.jpg") into preGeneRegions

	script:
	"""
	annotate.R $bed ${sampleID}_prePeakCalling_geneAnnotations.tsv ${sampleID}_prePeakCalling_geneRegions.tsv ${sampleID}_prePeakCalling_geneRegions.jpg TRUE
	"""
}

process annotate_genes_Post {
	publishDir "$outDir/OUTPUT", mode: 'copy', pattern: '*'
	container 'rcavalcante/bioconductor_docker:RELEASE_3_11'

	input:
	set sampleID, file(bed) from filteredPeaksGenes

	output:
	file("*.tsv") optional true into postGeneAnnot
	file("*.jpg") optional true into postGeneRegions

	script:
	"""
	if [ -s "$bed" ]; then
		annotate.R $bed ${sampleID}_filteredPeaks_geneAnnotations.tsv ${sampleID}_filteredPeaks_geneRegions.tsv ${sampleID}_filteredPeaks_geneRegions.jpg
	fi
	"""
}

process get_insertions_fasta {
	container 'biocontainers/bedtools:v2.27.1dfsg-4-deb_cv1'

	input:
	set sampleID, file(bed), file(genome) from filteredPeaksOut.combine( Channel.fromPath( params.genome ) )

	output:
	tuple sampleID, file("*_insertionsGenome.fasta"), env(meancov) into insertionsGenome

	script:
	"""
	bedtools getfasta -fi $genome -bed $bed -fo ${sampleID}_insertionsGenome.fasta
	meancov=`awk '{ sum += \$4 } END { if (NR > 0) print sum / NR }' $bed`
	if [ -z "\$meancov" ]; then
		meancov=0
	fi
	"""
}

process map_unanchored {
	container 'juliamp/minimap-samtools'

	input:
	set sampleID, file(genome), val(meancov) from insertionsGenome
	set sampleID, file(reads) from bamUnselected

	output:
	tuple sampleID, file("*_unselected_unmapped.fasta") into unanchored
	tuple sampleID, file("*_unselected_unmapped.fasta"), val(meancov) into unanchoredReads

	script:
	"""
	minimap2 -t 4 -ax map-ont $genome $reads -o ${sampleID}_unselected.sam
	samtools view -@ 4 -h -F 4 ${sampleID}_unselected.sam -o ${sampleID}_unselected_mapped.sam -U ${sampleID}_unselected_unmapped.sam
	samtools fasta -@ 4 -0 ${sampleID}_unselected_unmapped.fasta ${sampleID}_unselected_unmapped.sam
	"""
}


process hmmscan {
	container 'juliamp/unanchor'

	input:
	set sampleID, file(reads), val(meancov) from unanchoredReads
	tuple file(hmm), file(h3i), file(h3p), file(h3f), file(h3m) from Channel.fromPath( params.repeats ).collect()

	output:
	tuple sampleID, file(reads), file("*_hmmscan_besthit.tbl"), val(meancov) into mappedRepeats

	"""
	hmmscan -o ${sampleID}_hmmscan.txt --tblout ${sampleID}_hmmscan.tbl repeats.hmm $reads
	# Filtering only the best hit per read
	awk '!x[\$3]++' ${sampleID}_hmmscan.tbl > ${sampleID}_hmmscan_besthit.tbl
	"""
}


process call_unanchored {
	publishDir "$outDir/OUTPUT", mode: 'copy'
	container 'juliamp/unanchor'

	input:
	set sampleID, file(reads), file(tbl), val(meancov) from mappedRepeats

	output:
	tuple sampleID, file("*_unanchored_peaks.bed"), file("log_out.txt") into repeatsCalled
	tuple sampleID, env(unanchored) into unanchorCount

	script:
	"""
	mkdir tmp
	if [$meancov < 4]; then
		cov=$meancov
		threshold=\$(( \${cov%.*}*50/100 ))
	else
		threshold=2
	fi
	repeats_peak_calling.py --fasta $reads --tbl $tbl --tcov \$threshold -o ${sampleID}_unanchored_peaks.bed > log_out.txt
	unanchored=`wc -l ${sampleID}_unanchored_peaks.bed | cut -d" " -f1`
	"""
}

process output_table {
	input:
	set sampleID, val(mapped), val(pre), val(peaks), val(unanchor) from mappedCount.join( prepeaksCount ).join( peaksCount ).join( unanchorCount )

	output:
	file("*_output.csv") into outputReport

	script:
	"""
	echo "${sampleID};${mapped};${pre};${peaks};${unanchor}" >> ${sampleID}_output.csv
	"""
}

process write_output {
	publishDir "REPORT", mode: 'copy', pattern: '*.csv'

	input:
	file(out) from outputReport.collect()

	output:
	file("output.csv") into finalOutput

	script:
	"""
	for f in $out; do
		cat \$f >> output.csv
	done
	"""
}
*/
