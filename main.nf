#!/usr/bin/env nextflow

outDir = params.outDir

process adapter_transformation {
	input:
	set sampleID, val(ontarget), val(adapter_1F) from Channel.from( params.ontarget ).join( Channel.from( params.adapter_1F ) )
	val adapter_2 from params.adapter_2

	output:
	tuple sampleID, env(adapter_1m) into adapter1
	tuple sampleID, env(adapter_2m), env(ontarget_rc) into adapter2

	script:
	"""
	ontarget_rc=`echo $ontarget | tr ACGTacgt TGCATGCA | rev`
	adapter_1m=`echo $adapter_1F | tr acgt ACGT`
	adapter_2m=`echo $adapter_2 | tr acgt ACGT`
	"""
}

process stats {
	publishDir "$outDir/STATS", mode: 'copy'
	container 'juliamp/nanopack'

	input:
	set sampleID, file(reads) from Channel.fromPath( params.input ).map{ file -> tuple(file.baseName, file) }

	output:
	file("$sampleID/*.html") into html
	file("$sampleID/*.png") into png
	file("$sampleID/*.txt") into txt
	file("$sampleID/*.log") into log

	script:
	"""
    NanoStat --fastq $reads --outdir $sampleID --name $sampleID
    nanoQC -o $sampleID $reads
    NanoPlot --fastq $reads -o $sampleID
    """
}

process quality_filtering {
	publishDir "$outDir/FILTERED", mode: 'copy'
	container 'juliamp/nanopack'

	input:
	set sampleID, file(reads) from Channel.fromPath( params.input ).map{ file -> tuple(file.baseName, file) }

	output:
	tuple sampleID, file("*_qualityFilt.fastq") into filtered
	tuple sampleID, file("*_qualityFilt.fastq") into filteredNOclustered

	script:
	"""
	cat $reads | NanoFilt -l 200 -q 10 > ${sampleID}_qualityFilt.fastq   
	"""
}

process umi_extraction {
	publishDir "$outDir/UMI", mode: 'copy'
	container 'juliamp/pylibs'

	input:
	set sampleID, file(reads), val(adapter_1F) from filtered.join( adapter1 )
	val umiType from params.umiType

	output:
	tuple sampleID, file("*_extractedUMI.fasta") into extractedUmis

	script:
	if ( params.umiClustering )
	"""
	ONT_${umiType}_extract_umis.py --max-error 3 --adapter-length 250 \
		--fwd-context $adapter_1F \
		-o ${sampleID}_extractedUMI.fasta --tsv ${sampleID}_extractedUMI.tsv \
		$reads
	"""
	else
	"""
	touch dummy_extractedUMI.fasta
	"""
}

process umi_clustering {
	//publishDir "$outDir/UMI", mode: 'copy', pattern: '*_cluster_consensus.fasta'
	container 'biomehub/vsearch'

	input:
	set sampleID, file(umis) from extractedUmis
	val minlen from params.minlen
	val maxlen from params.maxlen
	val id from params.id

	output:
	tuple sampleID, file("${sampleID}_vsearch_clusters*") into clusters
	file("*_clusters_consensus.fasta") into consensus

	script:
	if ( params.umiClustering )
	"""
	vsearch --clusterout_id --clusters ${sampleID}_vsearch_clusters \
		--centroids ${sampleID}_clusters_centroid.fasta \
		--consout ${sampleID}_clusters_consensus.fasta \
		--minseqlength $minlen --maxseqlength $maxlen --qmask none --threads 4 \
		--cluster_fast $umis --clusterout_sort --gapopen 0E/5I --gapext 0E/2I \
		--mismatch -8 --match 6 --iddef 0 --minwordmatches 0 --qmask none -id $id
	"""
	else
	"""
	touch ${sampleID}_vsearch_clusters_dummy
	touch dummy_clusters_consensus.fasta
	"""
}

process umi_get_ubs {
	input:
	set sampleID, file(cluster) from clusters.transpose()

	output:
	tuple sampleID, env(size), file(cluster) into clusterTuples
	tuple sampleID, env(size), val(cluster.baseName) into getUBS

	script:
	if ( params.umiClustering )
	"""
	size=`grep -c "^>" $cluster`
	"""
	else
	"""
	size=1000000
	"""
}

ubsFile = outDir + "/ubs.txt"
cmd = "touch ${ubsFile}"
cmd.execute()
ubsFile = file(ubsFile)
ubsFile.append( getUBS.groupTuple( remainder:true ).collect().getVal() )

process umi_top_read {
	//publishDir "$outdir/UMI", mode: 'copy', pattern: '*'
	container 'juliamp/vsearchbiopy'

	input:
	set sampleID, val(size), file(cluster) from clusterTuples

	output:
	tuple sampleID, file("*consensus.fasta") optional true into consensusUmis
	tuple sampleID, file("top.fasta"), file("*seqs.fasta") optional true into topUmis

	when:
	size.toInteger() >= params.ubs

	script:
	if ( size.toInteger() == 1 )
	"""
	umi_to_seq.py -i $cluster -o ${cluster.baseName}_consensus.fasta
	"""
	else if ( params.umiClustering )
	"""
	umi_to_seq.py -i $cluster -o ${cluster.baseName}_seqs.fasta
	vsearch --sortbylength ${cluster.baseName}_seqs.fasta --topn 1 --output top.fasta
	sed -i 's/>/>centroid_/' top.fasta
	"""
	else
	"""
	touch dummy_consensus.fasta
	"""
}

process umi_polishing {
	//publishDir "$outdir/UMI", mode: 'copy', pattern: '*'
	container 'juliamp/racon-minimap'
	errorStrategy 'ignore'

	input:
	set sampleID, file(top), file(seqs) from topUmis

	output:
	tuple sampleID, file(seqs), file("racon_2.fa") into raconPolished

	script:
	"""
	minimap2 -x map-ont $top $seqs > ovlp_1.paf
	racon -t 4 -m 8 -x -6 -g -8 -w 500 --no-trimming $seqs ovlp_1.paf $top > racon_1.fa
	minimap2 -x map-ont racon_1.fa $seqs > ovlp_2.paf
	racon -t 4 -m 8 -x -6 -g -8 -w 500 --no-trimming $seqs ovlp_2.paf racon_1.fa > racon_2.fa
	"""
}

process umi_consensus {
	//publishDir "$outDir/UMI", mode: 'copy', pattern: '*'
	container 'juliamp/medaka'
	errorStrategy 'ignore'

	input:
	set sampleID, file(seqs), file(racon) from raconPolished

	output:
	tuple sampleID, file("*consensus.fasta") into medakaConsensus

	script:
	"""
	mini_align -i $seqs -r $racon -m -p map_1 -t 1
	medaka consensus map_1.bam consensus_1.hdf --threads 1 --model r941_min_high_g360 --chunk_len 6000
	mini_align -i $seqs -r $racon -m -p map_2 -t 1
	medaka consensus map_2.bam consensus_2.hdf --threads 1 --model r941_min_high_g360 --chunk_len 6000
	medaka stitch consensus_*.hdf ${seqs.baseName}_consensus.fasta
	"""
}

process umi_final {
	publishDir "$outDir/UMI", mode: 'copy', pattern: '*'
	beforeScript 'ulimit -Ss unlimited'

	input:
	set sampleID, file("*") from medakaConsensus.concat( consensusUmis ).groupTuple( remainder:true )

	output:
	tuple sampleID, file("${sampleID}_consensus.fasta") into finalConsensus

	script:
	"""
	find . -type l -name '*consensus.fasta' -exec cat {} + > ${sampleID}_consensus.fasta
	"""
}

process filtering_and_trimming {
	publishDir "$outDir/FILTERED", mode: 'copy'
	container 'genomicpariscentre/cutadapt:2.10'

	input:
	set sampleID, file(unclustered), file(clustered), val(adapter_2), val(ontarget) from filteredNOclustered.join( finalConsensus, remainder:true ).join( adapter2 )
	val error from params.error

	output:
	tuple sampleID, file("*_trimmed.fasta") into trimmed

	script:
	if ( params.umiClustering )
	"""
	ad=$adapter_2
	ot=$ontarget
	len=`expr \${#ad} + \${#ot}`
	cutadapt -e $error -O \$len --match-read-wildcards \
		--discard-untrimmed \
		-g $adapter_2...$ontarget --rc \
		-o ${sampleID}_trimmed.fasta \
		$clustered
	"""
	else
	"""
	ad=$adapter_2
	ot=$ontarget
	len=`expr \${#ad} + \${#ot}`
	cutadapt -e $error -O \$len --match-read-wildcards \
		--discard-untrimmed \
		-g $adapter_2...$ontarget --rc \
		-o ${sampleID}_trimmed.fasta \
		$unclustered
	"""
}

process mapping {
	publishDir "$outDir/MAPPED", mode: 'copy'
	container 'juliamp/minimap-samtools'

	input:
	set sampleID, file(reads) , file(genome) from trimmed.combine( Channel.fromPath( params.genome ) )

	output:
	tuple sampleID, file("*.sam") into mapped

	script:
	"""
	minimap2 -t 4 -ax map-ont $genome $reads -o ${sampleID}.sam
	"""
}

process map_filter {
	container 'juliamp/shapeilter'

	input:
	set sampleID, file(sam) from mapped

	output:
	tuple sampleID, file("*_filtered.sam") into filteredSam
	tuple sampleID, file("*_unselected.fasta") into bamUnselected

	script:
	"""
	filter_mapping.py -i $sam -p ${sampleID}_filtered.sam -s ${sampleID}_unselected.fasta
	"""
}

process create_bams {
	publishDir "$outDir/MAPPED", mode: 'copy', pattern: '*_filtered.sorted*'
	container 'juliamp/minimap-samtools'

	input:
	set sampleID, file(sam) from filteredSam

	output:
	tuple sampleID, file("*_filtered.sorted.bam"), file("*_filtered.sorted.bam.bai") into bamFiles

	script:
	"""
	samtools sort -@ 4 -O BAM -T tmp -o ${sampleID}_filtered.sorted.bam $sam
	samtools index -@ 4 ${sampleID}_filtered.sorted.bam ${sampleID}_filtered.sorted.bam.bai
	"""
}

process pre_peak_calling {
	publishDir "$outDir/PEAKS", mode: 'copy', pattern: '*_prePeakCalling_sorted_insertions.bed'
	publishDir "$outDir/PEAKS", mode: 'copy', pattern: '*_prePeakCalling.bed'
	container 'biocontainers/bedtools:v2.27.1dfsg-4-deb_cv1'
	containerOptions = "--user root"

	input:
	set sampleID, file(bam), file(bai) from bamFiles

	output:
	tuple sampleID, file("*_prePeakCalling_sorted_insertions.bed") into prePeakCallingInsGenes
	tuple sampleID, file("*_prePeakCalling_sorted_insertions.bed") into prePeakCallingInsRepeats
	tuple sampleID, file(bam), file(bai), file("*_prePeakCalling_sorted_insertions.bed") into peakCalling

	script:
	"""
	bedtools bamtobed -i $bam > ${sampleID}_prePeakCalling.bed
	bedtools merge -c 4 -o count -i ${sampleID}_prePeakCalling.bed > ${sampleID}_prePeakCalling_insertions.bed
	sort -r -nk4 ${sampleID}_prePeakCalling_insertions.bed > ${sampleID}_prePeakCalling_sorted_insertions.bed
	"""
}

params.tdif = null
params.tgen = null

process peak_calling {
	publishDir "$outDir/PEAKS", mode: 'copy', pattern: "*"
	container 'juliamp/shapeilter'

	input:
	set sampleID, file(bam), file(bai), file(bed), file(ref_vector) from peakCalling.combine( Channel.fromPath( params.refVector ) )

	output:
	tuple sampleID, file("${sampleID}_filtered_peaksSite.bed") into filteredPeaksRepeats
	tuple sampleID, file("${sampleID}_filtered_peaksSite.bed") into filteredPeaksGenes
	tuple sampleID, file("${sampleID}_filtered_peaks.bed") into filteredPeaksOut

	script:
	if( !params.tdif & !params.tgen )
	"""
	peak_calling.py --bam $bam --bed $bed \
		--oSite ${sampleID}_filtered_peaksSite.bed \
		--oFilt ${sampleID}_filtered_peaks.bed
	"""	
	else if ( !params.tdif )
	"""
		peak_calling.py --bam $bam --bed $bed \
		--oSite ${sampleID}_filtered_peaksSite.bed \
		--oFilt ${sampleID}_filtered_peaks.bed \
		--tgen $params.tgen
	"""
    else if ( !params.tgen )
   	"""
		peak_calling.py --bam $bam --bed $bed \
		--oSite ${sampleID}_filtered_peaksSite.bed \
		--oFilt ${sampleID}_filtered_peaks.bed \
		--tdif $params.tdif
	"""
	else
	"""
		peak_calling.py --bam $bam --bed $bed \
		--oSite ${sampleID}_filtered_peaksSite.bed \
		--oFilt ${sampleID}_filtered_peaks.bed \
		--tdif $params.tdif --tgen $params.tgen
	"""
}
/*
process annotate_repeats_Pre {
	publishDir "$outDir/OUTPUT", mode: 'copy', pattern: '*'
	container 'juliamp/annotation'

	input:
	set sampleID, file(bed) from prePeakCallingInsRepeats

	output:
	file("*.html") optional true into preRepeatAnnot

	script:
	"""
	annotate.py -i $bed -o ${sampleID}_prePeakCalling_repeatsAnnotations.html
	"""
}
*/
process annotate_repeats_Post {
	publishDir "$outDir/OUTPUT", mode: 'copy', pattern: '*'
	container 'juliamp/annotation'

	input:
	set sampleID, file(bed) from filteredPeaksRepeats

	output:
	file("*.html") optional true into postRepeatAnnot

	script:
	"""
	annotate.py -i $bed -o ${sampleID}_filteredPeaks_repeatsAnnotations.html --post
	"""
}

process annotate_genes_Pre {
	publishDir "$outDir/OUTPUT", mode: 'copy', pattern: '*'
	container 'rcavalcante/bioconductor_docker:RELEASE_3_11'

	input:
	set sampleID, file(bed) from prePeakCallingInsGenes

	output:
	file("*.tsv") into preGeneAnnot
	file("*.jpg") into preGeneRegions

	script:
	"""
	annotate.R $bed ${sampleID}_prePeakCalling_geneAnnotations.tsv ${sampleID}_prePeakCalling_geneRegions.tsv ${sampleID}_prePeakCalling_geneRegions.jpg TRUE
	"""
}

process annotate_genes_Post {
	publishDir "$outDir/OUTPUT", mode: 'copy', pattern: '*'
	container 'rcavalcante/bioconductor_docker:RELEASE_3_11'

	input:
	set sampleID, file(bed) from filteredPeaksGenes

	output:
	file("*.tsv") into postGeneAnnot
	file("*.jpg") into postGeneRegions

	script:
	"""
	annotate.R $bed ${sampleID}_filteredPeaks_geneAnnotations.tsv ${sampleID}_filteredPeaks_geneRegions.tsv ${sampleID}_filteredPeaks_geneRegions.jpg
	"""
}

process get_insertions_fasta {
	container 'biocontainers/bedtools:v2.27.1dfsg-4-deb_cv1'
	containerOptions = "--user root"

	input:
	set sampleID, file(bed), file(genome) from filteredPeaksOut.combine( Channel.fromPath( params.genome ) )

	output:
	tuple sampleID, file("*_insertionsGenome.fasta"), env(meancov) into insertionsGenome

	script:
	"""
	bedtools getfasta -fi $genome -bed $bed -fo ${sampleID}_insertionsGenome.fasta
	meancov=`awk '{ sum += \$4 } END { if (NR > 0) print sum / NR }' $bed`
	"""
}

process map_unanchored {
	container 'juliamp/minimap-samtools'

	input:
	set sampleID, file(genome), val(meancov) from insertionsGenome
	set sampleID, file(reads) from bamUnselected

	output:
	tuple sampleID, file("*_unselected_unmapped.fasta") into unanchored
	tuple sampleID, file("*_unselected_unmapped.fasta"), val(meancov) into unanchoredReads

	script:
	"""
	minimap2 -t 4 -ax map-ont $genome $reads -o ${sampleID}_unselected.sam
	samtools view -@ 4 -h -F 4 ${sampleID}_unselected.sam -o ${sampleID}_unselected_mapped.sam -U ${sampleID}_unselected_unmapped.sam
	samtools fasta -@ 4 -0 ${sampleID}_unselected_unmapped.fasta ${sampleID}_unselected_unmapped.sam
	"""
}


process hmmscan {
	container 'juliamp/unanchor'

	input:
	set sampleID, file(reads), val(meancov) from unanchoredReads
	tuple file(hmm), file(h3i), file(h3p), file(h3f), file(h3m) from Channel.fromPath( params.repeats ).collect()

	output:
	tuple sampleID, file(reads), file("*_hmmscan_besthit.tbl"), val(meancov) into mappedRepeats

	"""
	hmmscan -o ${sampleID}_hmmscan.txt --tblout ${sampleID}_hmmscan.tbl repeats.hmm $reads
	# Filtering only the best hit per read
	awk '!x[\$3]++' ${sampleID}_hmmscan.tbl > ${sampleID}_hmmscan_besthit.tbl
	"""
}


process call_unanchored {
	publishDir "$outDir/OUTPUT", mode: 'copy'
	container 'juliamp/unanchor'

	input:
	set sampleID, file(reads), file(tbl), val(meancov) from mappedRepeats

	output:
	tuple sampleID, file("*_unanchored_peaks.bed"), file("log_out.txt") into repeatsCalled

	script:
	"""
	mkdir tmp
	cov=$meancov
	threshold=\$(( \${cov%.*}*50/100 ))
	repeats_peak_calling.py --fasta $reads --tbl $tbl --tcov \$threshold -o ${sampleID}_unanchored_peaks.bed > log_out.txt
	"""
}
/*
process annotate_unmapped {
	publishDir "$outDir/OUTPUT", mode: 'copy'
	container 'dfam/tetools:latest'

	input:
	set sampleID, file(reads) from unanchored

	output:
	tuple sampleID, file("*.out") into repeatsUnmapped

	script:
	"""
	RepeatMasker -species human -engine hmmer -pa 8 $reads
	"""
}

process annotate_repeats_Unmapped {
	publishDir "$outDir/OUTPUT", mode: 'copy', pattern: '*'
	container 'juliamp/unanchor'
	errorStrategy 'ignore'

	input:
	set sampleID, file(out) from repeatsUnmapped

	output:
	file("*.tsv") into plotUnmapped

	script:
	"""
	call_Unanchored.py -i $out -o ${sampleID}_unmappedReads_repeatsAnnotations.tsv
	"""
}
*/