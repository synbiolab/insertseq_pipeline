# INSERTseq pipeline

## Introduction

INSETseq pipeline is a bioinformatics pipeline that can be used to analyse the landscape of genomic integrations from samples sequenced following INSERTseq library prep protocol.

The pipeline is built using Nextflow, a workflow tool to run tasks across multiple compute infrastructures in a very portable manner. It uses a Docker container making installation trivial and results highly reproducible. 
The implementation of this pipeline uses one container per process which makes it much easier to maintain and update software dependencies.

The interactive we aplication of INSERTseq is available at insertseq.com

## Quick Start

1. Install `Nextflow`
2. Install `Docker` for pipeline reproducibility. 
3. Download the pipeline from bitbucket and test it on a minimal dataset:
```{bash}
nextflow run https://bitbucket.org/synbiolab/insertseq_pipeline
```
4. Start running your own analysis.
4.1. Create your configuration file (i.e. `custom.config`) with the wanted parameters
4.2. Run the analysis
```{bash}
nextflow run https://bitbucket.org/synbiolab/insertseq_pipeline -c custom.config
```

Note that the pipeline will create the following files in your working directory:
```
work            # Directory containing the nextflow working files
results         # Results folder containing output files
.nextflow_log   # Log file from Nextflow
# Other nextflow hidden files.
```

## Creating a configuration file

An example configuration file `nextflow.config` has been created to run the pipeline with a minimal dataset. Default parameters are also provided in such file, but can be overwritten with a custom configuration file.

You can copy the provided configuration file and change the desired parameters or create a brand new custom configuration file containing only the modified paramters.

Note that, the parameter -c in nextflow will use the existing configuration file named `nextflow.config` and overwrite the variables present in the custom configuration file. To ignore the default configuration file, you can specify to use only the custom file with the parameter -C:
```{bash}
nextflow run https://bitbucket.org/synbiolab/insertseq_pipeline -C custom.config
```

### Parameters

The pipeline accepts different parameters which can be specified by using a configuration file. 

Parapeters that you will need to modify for your analysis:

- **outDir:**         Name of the of the output directory where output files will be stored. [testOutput]
- **input:**         Path where input samples are stored. It will analyse all files matching the pattern. [testDAta/\*sbset\*]
- **adapter_1F:**    Sequence of the adapter part before the UMI. It's provided in a list of lists data type to allow different adapters for each sample. The sample name corresponds to the file name without the extension. i.e. [ ["sample1", "ACTG"], ["Sample2", "GCTA"] ]
- **adapter_2:**     Sequence of the adapter part after the UMI. [ACACTCTTCACGCTCTTCCGATC]
- **ontarget:**       On-target primer to detect the integrated sequence and trim it. It's provided in a list of lists data type to allow different sequences for each sample, as adapter_1F.
- **umiClustering:**  Wether to perform an UMI clustering step or not. {true|false} [TRUE]
- **genome:**         Path to the reference genome file in fata format. We recommend to use an unmasked or soft masked genome for the analysis.

Advanced parameters that can be modified to customize the analysis:

- **umiType:**        Type of UMI used. It allows two different UMIs, TV (TTTVVVVTTVVVVTTVVVVTTVVVVTTT) or NYR (NNNYRNNNYRNNNYRNNN). [TV]
- **minlen:**         Minimum UMI length. By default one base less than the UMI length. [27]
- **maxlen:**         Maximum UMI length. By default one base more than the UMI length. [29]
- **id:**             Identity for UMI clustering. [0.99]
- **ubs:**            Threshold for UMI bin size. [1]
- **error:**          Error for adater pattern matching. [0.2]

Parameters needed for correct operation of the pipeline that should not be modified:

- **refVector:**      Path to a reference coverage profile of true peaks. Do not modify.
- **repeats:**        Path to Dfam repetitive region HMMs. Do not modify.
The default configuration file also contains the enabling of docker containers: `docker.enabled = true`

Parameters can also be provided by command line without the need of using a configuration file.
They can be provided with double dashes:
```{bash}
nextflow run https://bitbucket.org/synbiolab/insertseq_pipeline \
    --input "testDAta/*sbset*" \
    --genome ""Genomes/hg38/hg38_min.softmasked.fasta"" \
    --outDir "testOutput" \
    --adapter_1F = [["201212_2_subset", "AATGATACGGCGACCACCGAGATCTACACGAGTCTTGTGTCCCAGTTACCAGG"], ["201212_subset", "AATGATACGGCGACCACCGAGATCTACACGAGTCTTGTGTCCCAGTTACCAGG"]] \
	--adapter_2 = "ACACTCTTCACGCTCTTCCGATC" \
	--ontarget = [["201212_2_subset", "GTAACTAGAGATCCCTCAGACCCTTT"], ["201212_subset", "GTAACTAGAGATCCCTCAGACCCTTT"]]
```

## Results

Results are organized in folders.

- FILTERED
  + __\*\_qualityFilt.fastq:__               Quality filtered reads
  + __\*\_trimmed.fasta:__                   Trimmed reads
- MAPPED<br/>
  Contains SAM and BAM files for read alignment inspection. Alignments can be visualized with any genome browser program such as IGV or Geneious.
- PEAKS
  + __\*\_peaks.bed:__                             Bed file containing sigfinicant insertions. The columns correspond to chromosome, start, end, number of reads per insertion, DHD distance to a reference peak.
  + __\*\_filtered\_peakSite.bed:__                 Bed file containing significant insertions with only the predicted insertion point instead of a range.
  + __\*\_prePeakCalling\_sorted\_insertions.bed:__  Bed file with all genomic regions with mapped reads, contains significant and non significant insertions or noise.
- STATS<br/>
  Contains one folder per sample with histograms of raw read length or Nanopore sequencing quality.
- UMI
  + __\*\_consensus.fasta:__                       Contains consensus reads after UMI clustering.
  + __\*\_extractedUMI.fasta:__                    Contains extracted UMIs from each read.
- OUTPUT<br/>
  Contains plots and tables of gene feature annotations and unanchored peak calling.
  + __\*\_unanchored_peaks.bed:__                  Bed file containing unanchored insertions.

## Updating the pipeline

The typical command for running the pipelin is as follows:

```{bash}
nextflow run synbiolab/insertseq_pipeline -c custom.config
```

Nextflow will automatically pull the pipeline code from Bitbucket and store it as a cached version. Even if the pipeline has been updated, Nextflow will always use the cached version if available. To update the pipeline to the latest version, you can update the cached version using:

```{bash}
nextflow pull synbiolab/insertseq_pipeline
```

## Translocations

INSERTseq pipeline can also be used to detect translocations.

```{bash}
nextflow run insertseq_TRANSLOC.nf -c Translocations-example/translocation-test.config
```

## Maintainance and Support

For further information or help, do not hesitate to get in touch with julia.mir@upf.edu

## Citation
Ivančić, D., Mir-Pedrol, J., Jaraba-Wallace, J., Rafel, N., Sanchez-Mejias, A., & Güell, M. (2022). INSERT-seq enables high-resolution mapping of genomically integrated DNA using Nanopore sequencing. Genome Biology, 23(1), 1-13.
