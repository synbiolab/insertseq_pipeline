#!/usr/bin/env python

def reformatUMIfasta(umiFasta, inReads, outName):
    fasta = SeqIO.parse(inReads, "fastq")
    reads = SeqIO.to_dict(fasta)
    umis = SeqIO.parse(umiFasta, "fasta")
    out = open(outName, 'w')

    for umi in umis:
        seq = str(reads[umi.id].seq)
        out.write(">{};strand=+;umi_fw=0;umi_rev=0;umi_fw_seq=;umi_rev_seq={};seq={}\n{}\n".format(id, umi, seq, umi))
    
    out.close()




if __name__ == "__main__":
    from Bio import SeqIO
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("-i", help="Fasta file containing UMIs", action="store", required=True)
    parser.add_argument("-o", help="Output fasta file to contain sequences", action="store", required=True)
    args = parser.parse_args()

    UmiToSeq(args.i, args.o)
    
