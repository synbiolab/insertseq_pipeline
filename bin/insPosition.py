#!/usr/bin/env python

def retrieveInsPosition(bamFile, bedFile):
	"""Collect insertions from bed file. Output position with highest coverage"""
	bam = pysam.AlignmentFile(bamFile, "rb")
	pysam.tabix_index(bedFile, seq_col=0, start_col=1, end_col=2, keep_original=True)
	bed = pysam.TabixFile(bedFile + ".gz")
	positions = []

	for i in bed.fetch():
		i = i.split("\t")
		pileup = bam.pileup(i[0], int(i[1]), int(i[2]))
		covs = [[p.get_num_aligned(), p.pos, p.reference_name] for p in pileup] # coverage, position, chromosome
		maxcov = max(covs, key=lambda x: x[0]) # get maximum coverage
		positions.append([maxcov[2], maxcov[1]]) #chromosome, position

	return positions

def writeOutput(outName, positions):
	"""Write bed file with retrieved insertion position"""
	with open(outName, "w") as outFile:
		for pos in positions:
			outFile.write("{}\t{}\t{}\n".format(pos[0], pos[1], pos[1]))


if __name__ == "__main__":
	import pysam
	import argparse

	parser = argparse.ArgumentParser()
	parser.add_argument("--bam", help="Input bam file with mapped reads", action="store", required=True)
	parser.add_argument("--bed", help="Input bed file with insertions", action="store", required=True)
	parser.add_argument("-o", help="Output bed file name", action="store", required=True)
	args = parser.parse_args()

	writeOutput(args.o, retrieveInsPosition(args.bam, args.bed))
