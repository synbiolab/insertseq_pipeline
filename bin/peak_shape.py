#!/usr/bin/env python

def parse_bed(bedFile):
    """Obtain peak regions from bed file"""
    insertions = {}
    with open(bedFile) as bed:
        for line in bed:
            line = line.strip()
            line = line.split("\t")
            insertions[line[0]+":"+line[1]+"-"+line[2]] = [line[0], line[1], line[2], line[3], line[4], line[5], line[6], line[7], line[8], line[9]]
    return(insertions)

def obtain_mapped_readLengths(bam, insertions):
    """Obtain length of mapped reads per insertion"""
    lengths = {}
    bamFile = pysam.AlignmentFile(bam, "rb")
    for ins in insertions.keys():
        reads = bamFile.fetch(insertions[ins][0], int(insertions[ins][1]), int(insertions[ins][2]), until_eof=True)
        lengths[ins] = []
        for r in reads:
            lengths[ins].append(r.query_alignment_length)
        lengths[ins] = np.array(lengths[ins])
    return(lengths)


def filter_reads(lengths, distribution="gamma", threshold=0.0007547906866842447):
    """Select only insertions with propper read length distribution"""
    filtered = []
    for ins in lengths.keys():
        fit = distfit(distr = distribution)
        fit.fit_transform(lengths[ins])
        if float(fit.summary.RSS) <= threshold:
            filtered.append(ins)
    return(filtered)

def print_output(insertions, filtered, output):
    """Print bed file containing only insertions passing shape filter"""
    with open(output, 'w') as out:
        for f in filtered:
            out.write("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\n".format(*insertions[f]))


if __name__ == "__main__":
    import pysam
    from distfit import distfit
    import numpy as np
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("--bam", help="Bam file of mapped reads", action="store", required=True)
    parser.add_argument("--bed", help="Bed file with stignificant called peaks", action="store", required=True)
    parser.add_argument("-o", help="Output bed file with peaks passing shape filter", action="store", required=True)
    args = parser.parse_args()

    insertions = parse_bed(args.bed)
    print_output(insertions, filter_reads(obtain_mapped_readLengths(args.bam, insertions)), args.o)
    
